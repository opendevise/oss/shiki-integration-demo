'use strict'

const shiki = require('shiki')

module.exports.register = function () {
  this.on('contextStarted', async () => {
    global.highlighter = await shiki.getHighlighter({ theme: 'nord', langs: ['ruby' ] })
  })
}
