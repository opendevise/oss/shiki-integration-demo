'use strict'

const shiki = require('shiki')

const START_RX = /<pre class="shiki" style="background-color: #[0-9a-f]*"><code>/
const END = '</code></pre>'

function registerSyntaxHighlighter () {
  const Opal = global.Opal
  const Asciidoctor = Opal.Asciidoctor
  const classDef = Opal.klass(null, Asciidoctor.SyntaxHighlighter.Base, 'ShikiSyntaxHighlighter')
  Opal.defn(classDef, '$highlight?', function isHighlight () {
    return true
  })
  Opal.defn(classDef, '$highlight', function highlight (node, source, lang, opts) {
    return global.highlighter.codeToHtml(source, { lang }).replace(START_RX, '').slice(0, -END.length)
  })
  Asciidoctor.SyntaxHighlighter.$register(classDef, 'shiki')
}

function registerSyntaxHighlighterPreprocessor () {
  return this.process((doc, reader) => {
    const Opal = global.Opal
    const Asciidoctor = Opal.Asciidoctor
    if (Asciidoctor.SyntaxHighlighter.$for('shiki') === Opal.nil) registerSyntaxHighlighter()
    return reader
  })
}

function toProc (fn) {
  return Object.defineProperty(fn, '$$arity', { value: fn.length })
}

module.exports.register = function (registry) {
  if ('groups' in registry) {
    // Antora
    registry.groups.$send('[]=', 'shiki_syntax_highlighter', toProc(function () {
      this.preprocessor(registerSyntaxHighlighterPreprocessor)
    }))
  } else {
    // standalone Asciidoctor
    registry.register(function () {
      this.preprocessor(registerSyntaxHighlighterPreprocessor)
    })
  }
}
